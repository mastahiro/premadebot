module.exports = {
	name: 'editembedmessage',
	description: 'Tells the bot what channel to edit the embbed',
	cooldown: 1,
	execute(message, args) {
		const fs = require('fs');
		const Discord = require('discord.js');

		var rawdata = fs.readFileSync('./data/queue.json');
		let queueobj = JSON.parse(rawdata);
		let channelid = queueobj.embedchannelid;

		const channeltosend = message.guild.channels.resolve(channelid);
		if(!channeltosend) return message.channel.send('Saved channel data is invalid');

		channeltosend.messages.fetch(queueobj.embedid).then(msg=> {
			const originalEmbed = msg.embeds[0];
			let newEmbed = new Discord.MessageEmbed(originalEmbed).setTitle(args[0]);
			msg.edit(newEmbed);
		});
	},
};
