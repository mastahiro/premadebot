module.exports = {
	name: 'ping',
	description: 'Ping!',
  aliases:['pong'],
  cooldown: 10,
	execute(message, args) {
		message.channel.send('Pong.');
	},
};
