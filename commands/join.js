module.exports = {
	name: 'join',
	description: 'Joins the player into a queue',
	cooldown: 10,
	execute(message, args) {
		const fs = require('fs');

		var rawdata = fs.readFileSync('./data/players.json');
		let players = JSON.parse(rawdata);

		const index = players.findIndex(i=>i.id = message.author.id);
		if(index < 0) {
			return message.reply('you need to set your spec first, do `!spec <name>`');
		}

		rawdata = fs.readFileSync('./data/queue.json');
		let queueobj = JSON.parse(rawdata);
		let queue = queueobj.queue;


		console.log(queue.indexOf(message.author.id));
		if(queue.indexOf(message.author.id) >= 0) return message.reply('you are already in queue');
		queue.push(message.author.id);

		let data = JSON.stringify(queueobj);
		fs.writeFileSync('./data/queue.json', data);

		console.log(queue);
		message.reply('you\'ve been added to the lobby');
	},
};
