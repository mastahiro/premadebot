module.exports = {
	name: 'spec',
	description: 'Specifies the class spec',
	args: true,
	aliases: ['setspec','new','set'],
  cooldown: 10,
	execute(message, args) {
		//TODO: Filter specs
    const fs = require('fs');

    const rawdata = fs.readFileSync('./data/players.json');
    let players = JSON.parse(rawdata);

		const index = players.findIndex(i=>i.id = message.author.id);
		console.log(index);
    if(index >= 0) players.splice(index, 1);
		var player = {
			id: message.author.id,
			spec: args[0]
		}
    players.push(player);

    let data = JSON.stringify(players);
    fs.writeFileSync('./data/players.json', data);

    console.log(players);
    message.reply(`spec added successfully`);
	},
};
