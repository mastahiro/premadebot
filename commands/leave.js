module.exports = {
	name: 'leave',
	description: 'Joins the player into a queue',
	alias: ['quit', 'exit'],
	cooldown: 0,
	execute(message, args) {
		const fs = require('fs');

		const rawdata = fs.readFileSync('./data/queue.json');
		let queueobj = JSON.parse(rawdata);
		let queue = queueobj.queue;

		const index = queue.indexOf(message.author.id);
		if(index < 0) return message.reply('you are not in queue');
		queue.splice(index, 1);

		let data = JSON.stringify(queueobj);
		fs.writeFileSync('./data/queue.json', data);

		message.reply('you\'ve been removed from queue');
	},
};
