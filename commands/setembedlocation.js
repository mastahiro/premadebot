module.exports = {
	name: 'setembedlocation',
	description: 'Tells the bot what channel to make the embbed',
	cooldown: 1,
	execute(message, args) {
		const fs = require('fs');
		const Discord = require('discord.js');

		if(!message.guild.channels.resolve(args[0])) return message.reply('that is not a valid channel ID or the channel does not exist');
		const channeltosend = message.guild.channels.resolve(args[0]);

		const exampleEmbed = new Discord.MessageEmbed()
			.setColor('#0099ff')
			.setTitle('Some title')
			.setURL('https://discord.js.org/')
			.setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
			.setDescription('Some description here')
			.setThumbnail('https://i.imgur.com/wSTFkRM.png')
			.addFields(
				{ name: 'Regular field title', value: 'Some value here' },
				{ name: '\u200B', value: '\u200B' },
				{ name: 'Inline field title', value: 'Some value here', inline: true },
				{ name: 'Inline field title', value: 'Some value here', inline: true },
			)
			.addField('Inline field title', 'Some value here', true)
			.setImage('https://i.imgur.com/wSTFkRM.png')
			.setTimestamp()
			.setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png');

		channeltosend.send(exampleEmbed).then(msg=> {
			var rawdata = fs.readFileSync('./data/queue.json');
			let queueobj = JSON.parse(rawdata);

			queueobj.embedchannelid = channeltosend.id;
			queueobj.embedid = msg.id;

			let data = JSON.stringify(queueobj);
			fs.writeFileSync('./data/queue.json', data);
			console.log(msg);

			message.reply('I have saved that location and created the appropriate message');
		});
	},
};
