# PremadeBot for Discord

### What is this repository for? ###

* A queue bot for Discord that lets people join and leave queue for premade PVP Content.

### How do I get set up? ###

* Install Node.js
* Create a discord bot and make a `config/botconfig.json` file with the following format:
```
{
  "prefix":"!",
  "clientid":"<id>",
  "clientSecret":"<secret>",
  "token":"<token>"
}
```
(You can get these strings once you create a Discord bot)

If it doesn't run after that try the following:

* Install Discord.js through NPM (I think it might not be necessary)

Other useful stuff: [Discord.Js Tutorial](https://discordjs.guide/)


### Dependencies
* Discord.js

### Who do I talk to? ###

* Discord: Mastahiro#0548 | Email: mastahiro@hotmail.com
* <At Least We Have Stables> [Discord channel](https://discord.gg/KFjwjkN)
